# Local IspellDict: de
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2018-2021 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+KEYWORDS: HTML, CSS, JavaScript, DOM,

* HTML
Die [[https://de.wikipedia.org/wiki/Hypertext_Markup_Language][Hypertext Markup Language (HTML)]]
ist /die/ Sprache (genauer: Sprachfamilie) des Web, die typischerweise
in Kombination mit CSS und JavaScript zum Einsatz kommt.  Das
[[https://wiki.selfhtml.org/wiki/Startseite][selfHTML-Wiki]] ist
/die/ deutschsprachige OER rund um HTML.

Lesen Sie
[[https://wiki.selfhtml.org/wiki/HTML/Tutorials/Trennung_von_Inhalt,_Pr%C3%A4sentation_und_Verhalten][im selfHTML-Wiki]],
wie mit diesen drei Sprachen Inhalt, Präsentation und Verhalten getrennt
werden, und *erklären* Sie danach
was unter „Separation of Concerns“ verstanden wird, welche Vorteile
die Einhaltung dieses Prinzips verspricht und wie es mit der Trennung
von Inhalt, Präsentation und Verhalten im Kontext von HTML, CSS und
JavaScript zusammenhängt.

Wenn Sie noch keine Erfahrung mit HTML haben, empfehle ich
[[https://wiki.selfhtml.org/wiki/HTML/Tutorials/Einstieg][diesen HTML-Einstieg im selfHTML-Wiki]],
der auch Grundlagen von CSS behandelt.
Weitergehende Details zu CSS finden sich dann im
[[https://wiki.selfhtml.org/wiki/CSS/Tutorials/Einstieg][Einstieg in CSS]].
Um die Grundlagen von JavaScript zu erlernen, können Sie das
[[https://www.jshero.net/][Tutorial JavaScript Hero]] absolvieren.
(Im Kontext dieses Vertiefungsmoduls ist das allerdings nicht nötig.
Die weiter unten angegebenen Texte des selfHTML-Wiki zum DOM sollten
Ihnen helfen, die folgenden Aufgaben zu lösen.)

Im Zuge der Web-Entwicklung können Sie beispielsweise per
[[https://oer.gitlab.io/oer-courses/vm-oer/04-Docker.html#slide-docker-nginx][Docker]]
einen Web-Server auf dem eigenen Rechner betreiben, der während der
Entwicklung Ihre Web-Ressourcen ausliefert.

Zudem existieren Online-Editoren, die gleichzeitig sowohl den
Quelltext einer Web-Seite als auch ihre Browser-Darstellung live
anzeigen.  In sehr einfacher Form funktioniert das {{{zb}}} in
HTML-Dokumenten, in denen das Plugin
[[https://github.com/viebel/klipse][Klipse]] eingebettet ist.
Umfangreichere Funktionalität bieten etwa
[[https://jsbin.com/][JS Bin]]
([[https://github.com/jsbin/jsbin][v4 als freie Software]])
oder proprietäre Alternativen wie [[https://jsfiddle.net/][JSFiddle]]
([[https://jsfiddle.net/SELFHTML/tvxTg/][Hello-World-Beispiel von selfHTML]])
und [[https://codepen.io/][CodePen]].

Schließlich sei auf die *Web Developer Tools* moderner Browser
hingewiesen, die Sie über das Menü oder Shortcuts aufrufen können.
Probieren Sie im Firefox oder Chromium ~Strg-Umschalt-I~, und schauen
Sie sich den HTML-Text dieser Seite an (Reiter Inspector im Firefox,
Elements im Chromium; in weiteren Reitern können Sie sich
beispielsweise Netzwerkaufrufe einschließlich der HTTP-Header und
Cookies ansehen).  Beachten Sie, wie im Kopf des HTML-Textes
(~<head>~) CSS und JavaScript eingebunden werden; im Körper
(~<body>~) finden Sie diesen Text gefolgt von weiterem JavaScript.

* JavaScript, DOM
JavaScript ist /die/ Programmiersprache des Web.  JavaScript-Programme
können in HTML-Dokumente eingebunden und dann vom Browser ausgeführt
werden.  Sie können dann {{{zb}}} das HTML-Dokument dynamisch anpassen
oder im Hintergrund Daten mit Servern im Internet austauschen.  Die
Möglichkeiten sind nahezu unbegrenzt und können sowohl für gute Zwecke
ge- als auch Kriminelles missbraucht werden.

Als Schnittstelle zwischen JavaScript und HTML dient das
Document Object Model (DOM), zu dem das selfHTML-Wiki eine
[[https://wiki.selfhtml.org/wiki/JavaScript/DOM][Kurzerläuterung]]
und ein
[[https://wiki.selfhtml.org/wiki/JavaScript/Tutorials/DOM][etwas längeres Tutorial]]
liefert.

# Local Variables:
# indent-tabs-mode: nil
# End:
