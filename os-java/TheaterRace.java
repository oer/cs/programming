/*
  Code to demonstrate races.

  SPDX-FileCopyrightText: 2018, 2021 Jens Lechtenbörger
  SPDX-License-Identifier: CC-BY-SA-4.0 OR GPL-3.0-or-later

  The body of method BuggyTheater.sell() is based on code from Sec. 4.2 of
  this book: Max Hailperin, Operating Systems and Middleware – Supporting Controlled Interaction, https://gustavus.edu/mcs/max/os-book/, CC BY-SA 3.0.
  The (broken) flag locked is not part of the book's code.
  This file was derived from that one:
  https://gitlab.com/oer/OS/blob/master/java/TheaterEx.java
*/
public class TheaterRace { // A main method to create a theater and several threads.
    private static int noThreads = 100;
    private static int noSeats = 50;
    public static void main(String[] args) {
        if (args.length == 2) {
            noSeats = Integer.parseInt(args[1]);
        }
        if (args.length > 0) {
            noThreads = Integer.parseInt(args[0]);
        }

        BuggyTheater theater = new BuggyTheater(noSeats);

        // Create noThreads customers, each of which will try to buy a ticket
        // at the same theater.  Clearly, this should not be possible if
        // noThreads > noSeats.
        Thread[] customers = new Thread[noThreads];
        for (int i=0; i<noThreads; i++)
            customers[i] = new Thread(new RacingCustomer(theater));

        // Start the customers.
        // To the reader: Carefully check the output messages.  Where do you
        // see the first surprise (if any)?
        for (int i=0; i<noThreads; i++)
            customers[i].start();
    }
}

class RacingCustomer implements Runnable { // Each customer tries to buy one seat.
    private BuggyTheater theater;
    public RacingCustomer(BuggyTheater theater) {
        this.theater = theater;
    }
    public void run() {
        int remaining = theater.sell();
        if (remaining == -1) System.out.println("All tickets sold out.");
        else System.out.println("Got ticket.  Remaining: " + remaining);
        }
}

/**
   This code serves as bad example.
   The boolean flag <code>locked</code> is misused as broken MX mechanism.  It
   is meant to function as lock: unlocked (false) in constructor (l. 93) and
   locked for the duration of the critical section (true from l. 79 to l. 89).
   Before entering the critical section (l. 77), test wether
   <code>locked</code> is true; if so, wait in while loop for lock to be
   released (because some other thread left its critical section).
   You are supposed to figure out yourself what is broken here.
   (Side note: <code>locked</code> is <code>volatile</code> to synchronize
   reads and writes of that variable; without it, a cached value could be
   reused in  <code>while (locked);</code>, leading to an infinite loop.
   Feel free to experiment with and without <code>volatile</code>.  As you
   will see yourself, <code>volatile</code> is not good enough.  In class, we
   use proper MX mechanisms such as <code>synchronized</code>, and we do not
   need <code>volatile</code>.)
*/
class BuggyTheater {
    int seatsRemaining;
    volatile boolean locked;

    public int sell() {
        String name = Thread.currentThread().getName(); // Each thread has a name.
        System.out.printf("%s before lock check.%n", name);
        while (locked); // Wait here as long as locked is true.
        System.out.printf("%s beyond lock check.%n", name);
        locked = true;  // Lock theater.
        System.out.printf("%s locked theater with %d remaining seats.%n", name, seatsRemaining);
        int result = -1;
        if (seatsRemaining > 0) {
            dispenseTicket();
            seatsRemaining = seatsRemaining - 1;
            result = seatsRemaining;
        } else {
            displaySorrySoldOut();
        }
        locked = false; // Unlock theater.
        System.out.printf("%s released theater.%n", name);
        return result;
    }
    public BuggyTheater(int seats) { locked = false; seatsRemaining = seats; }
    private void dispenseTicket() { System.out.println("Selling ticket when " + seatsRemaining + " are remaining."); }
    private void displaySorrySoldOut() { System.out.println("Sorry, everything sold out."); }
}
