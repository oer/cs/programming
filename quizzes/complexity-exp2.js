// SPDX-FileCopyrightText: 2022 Jens Lechtenbörger
// SPDX-License-Identifier: CC-BY-SA-4.0

quizComplexityExp2 = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Multiply variable times for exponents.",
        "level1":  "Excellent!",     // 80-100%
        "level2":  "Maybe ask for help?", // 60-79%
        "level3":  "Maybe ask for help?", // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Maybe ask for help?"  // 0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements when arguments are exchanged.",
            "a": [
                {"option": "naive_exp(op1,op2) = op1^op2", "correct": true},
                {"option": "We know that naive_mult(op1,op2) is linear in the second argument.", "correct": true},
                {"option": "With the change, the second argument is not fixed any more but grows with the accumulated intermediate result.", "correct": true},
                {"option": "As the intermediate result approaches the final result, the number of plus operations grows exponentially.", "correct": true},
                {"option": "I changed the code.  Then, I computed 2^19, 2^20, and 2^21.  Doing so, I observed roughly doubling times myself.", "correct": true},
            ],
            "correct": "<p><span>Very good!  This small change turns our algorithm into an exponential one.</span></p>",
            "incorrect": "<p>Do you need help?  All statements are correct.  Which one confuses you?</p>" // no comma here
        }
    ]
};
