// SPDX-FileCopyrightText: 2022 Jens Lechtenbörger
// SPDX-License-Identifier: CC-BY-SA-4.0

quizComplexityMult = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Add several times to multiply once.",
        "level1":  "Excellent!",     // 80-100%
        "level2":  "Maybe ask for help?", // 60-79%
        "level3":  "Maybe ask for help?", // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Maybe ask for help?"  // 0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about the result of naive_mult.",
            "a": [
                {"option": "naive_mult(op1,0) = 0", "correct": true},
                {"option": "naive_mult(op1,1) = op1", "correct": true},
                {"option": "naive_mult(op1,5) = op1+op1+op1+op1+op1 = op1*5", "correct": true},
                {"option": "naive_mult(op1,op2) = op1*op2", "correct": true}
            ],
            "correct": "<p><span>Very good!  This algorithm indeed multiplies is arguments by repeated addition.</span></p>",
            "incorrect": "<p>Do you need help?  All statements are correct.  Which one confuses you?</p>" // no comma here
        },
	{
            "q": "Select correct statements about plus operations in naive_mult.",
            "a": [
                {"option": "naive_mult(op1,1) does not require a plus operation.", "correct": true},
                {"option": "3*5 = 3+3+3+3+3.  Thus, we can add 5-1 = 4 times to multiply by 5.", "correct": true},
                {"option": "The first argument does not influence the number of plus operations.", "correct": true},
                {"option": "The algorithm needs op2-1 plus operations, i.e., the number of plus operations is linear in the second argument.", "correct": true}
            ],
            "correct": "<p><span>Very good!  This algorithm has a linear complexity of O(op2).</span></p>",
            "incorrect": "<p>Do you need help?  All statements are correct.  Which one confuses you?</p>" // no comma here
        }
    ]
};
