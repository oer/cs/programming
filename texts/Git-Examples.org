# Local IspellDict: en
#+SPDX-FileCopyrightText: 2022,2023 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+KEYWORDS: OER, GitLab, git, demo, example, exercise, fork, clone, branch, feature branch workflow,

* Context
:PROPERTIES:
:CUSTOM_ID: sec-intro
:END:
In 2021, several students in [[https://oer.gitlab.io/oer-courses/cacs/][CACS]]
suggested that I create a short video for Git, demonstrating basic
concepts before details show up in the corresponding
[[https://oer.gitlab.io/oer-courses/cacs/Git-Introduction.html][presentation]]
and
[[https://oer.gitlab.io/oer-courses/cacs/texts/Git-Workflow-Instructions.html][exercises]].

I’m not a fan of videos myself.  This document
([[https://gitlab.com/oer/cs/programming/-/blob/master/texts/Git-Examples.org][source code]])
is a script for [[https://oer.gitlab.io/oer-courses/cacs/videos/Git-Examples.mp4][that video]],
which you can find on the course page.
In my view, the script is superior to the video: It contains working
hyperlinks, enables copy&paste, searching, annotations, skim reading,
you do not need to decode my pronunciation, and text
automatically adjusts to your individual speed.
(An advantage of the video is that you can see me smiling.  Sometimes. 😃)

Briefly, Git is a decentralized version control system, which supports
collaboration on a collection of documents, keeping track of changes
and versions over time.  Let’s look at two examples: First, LaTeX
source code for a textbook on GitHub, second JavaScript code used
in my presentations on GitLab.

* Collaboration on OER Textbook on GitHub
:PROPERTIES:
:CUSTOM_ID: sec-github-textbook
:END:

The LaTeX source files for a textbook on operating systems
([[https://oer.gitlab.io/OS/][used at the University of Münster]])
are maintained in a
[[https://github.com/Max-Hailperin/Operating-Systems-and-Middleware--Supporting-Controlled-Interaction][Git repository on GitHub]].

Thus, if you as student or I as instructor find a typo or believe to
be able to explain a certain concept in a better way, we can create an
improved version and ask the author to incorporate our changes into
the book.  On GitHub, so-called /pull requests/ (PRs) are used for such
collaborations, while /merge request/ (MR) is the term used with GitLab for
the same purpose.
[[https://github.com/Max-Hailperin/Operating-Systems-and-Middleware--Supporting-Controlled-Interaction/pull/120][See here for a sample PR.]]

[[./github-pr-hailperin.png]]

You will learn details of the so-called [[https://oer.gitlab.io/oer-courses/cacs/Git-Introduction.html#slide-feature-branch-workflow][feature branch workflow]]
for such collaborations.  Briefly, /branches/ are different versions of
the repository, and a /feature branch/ is a branch that aims to handle a
specific feature or issue.  Frequently, the ~main~ or ~master~ branch
represents a stable version (although teams are free to name their
branches as they want; also, any number of branches may be used, say
for development, testing, releases).  In any case, feature branches
contain work-in-progress until they are /merged/ into the stable
version.  For the sample PR you see that the book’s author, Max
Hailperin, accepted it and merged the branch ~deadlock~ created by
user ~lechten~, that’s me, into the stable version. 😃

Note that Git comes with /diff/ functionality to highlight differences
between versions, which simplifies review processes.

[[./github-pr-hailperin.png]]


Collaboration is particularly attractive if
[[https://oer.gitlab.io/OS/Operating-Systems-Motivation.html#slide-free-software][freedom granting licenses]]
are being used.  In case of educational resources such as this textbook, we
then talk about Open Educational Resources, OER for short.

Please feel free to collaborate in a similar fashion on our OER. 😃

Let’s turn to the second example.

* Collaboration on Free Software on GitLab
:PROPERTIES:
:CUSTOM_ID: sec-gitlab-feature-branch
:header-args: :session  fix
:header-args: :eval no-export
:header-args: :cache yes
:END:

While Git is sometimes being used for educational resources, it is
more popular for software development, again particularly when free
and open licenses are being used.  If you are using such software and
you find a bug or would like to add a new feature, you can create an
improved version of that software and suggest the inclusion of your
improvements into the original version.  This can happen using the
feature branch workflow that I talked about already.

Let’s see a step-by-step example for the feature branch workflow with
Git and GitLab.  I use the JavaScript presentation framework
[[https://revealjs.com/][reveal.js]]
for my OER presentations.  In addition, I use a
[[https://gitlab.com/schaepermeier/reveal.js-quiz][quiz plugin]]
for  reveal.js, which enables students to check their understanding and
supports their learning with automatic feedback.  In that plugin, I
discovered an incompatibility with a recent change in reveal.js, which
I want to fix now.  Here, you see the GitLab repository of that
plugin, which, by the way, was created by a former student of mine in
the context of a Bachelor’s specialization module. 😃

1. For demo purposes, I use a new empty directory (and set ~LANG~ for
   English messages as well as ~PAGER~ to prevent pagination when
   capturing long results—you do not need those ~export~ commands):
   #+begin_src sh :cache no
     mkdir -p /tmp/git-demo
     cd /tmp/git-demo
     export LANG=en_US.UTF-8
     export PAGER=cat
   #+end_src

2. On GitLab I do not have special permissions on the plugin’s
   repository.  Thus, I first /fork/ the repository.  Forking means
   that I create a copy of the project with which I can do whatever I
   want.  I already forked the project in the past, so now I just go
   to [[https://gitlab.com/oer/reveal.js-quiz][my fork]].

3. I obtain a local copy of the source code, so that I can change
   files with the editor of my choice afterwards.  The Git operation
   for copying is called /clone/, for which a GitLab GUI button
   provides repository addresses.

   [[./gitlab-clone-addresses.png]]

   Usually, the HTTPS address is meant for anonymous read access by
   the general public, while the SSH address requires a user account
   on the platform.  As I want to change my fork, I use the SSH
   address subsequently.  (If you want to try out the below commands
   without a GitLab account, you need the HTTPS address.  However,
   then you will not be allowed to push changes in step 7 below.
   In exercises, you will work with an account...)

   #+begin_src sh :results output :exports code :cache yes
     git clone git@gitlab.com:oer/reveal.js-quiz.git
   #+end_src

   Output:
   #+RESULTS[353a3fe4a0968cc9c5478cb0fa256ac5c5d892a7]:
   : Cloning into 'reveal.js-quiz'...
   : remote: Enumerating objects: 683, done.
   : remote: Counting objects: 100% (683/683), done.
   : remote: Compressing objects: 100% (283/283), done.
   : remote: Total 683 (delta 415), reused 658 (delta 397), pack-reused 0
   : Receiving objects: 100% (683/683), 350.96 KiB | 1.57 MiB/s, done.
   : Resolving deltas: 100% (415/415), done.

   Note that the cloned source code now exists in a new directory,
   ~cd~ (change directory) into it, and ~ls~ (list files):
   #+begin_src sh :results output :exports code :cache yes
     cd reveal.js-quiz
     ls
   #+end_src

   Output:
   #+RESULTS[3e0dffdbbb652b8f20e99c44da5db33ce7b89cf2]:
   :
   : quiz  README.md

4. I create a new branch for my bugfix with the command
   ~git checkout -b~.  As the bug is related to the propagation of
   events, I call the branch ~fix-event-propagation~.
   #+begin_src sh :exports code :cache yes
     git checkout -b fix-event-propagation
   #+end_src

   Output:
   #+RESULTS[83ffcf64b7a3f96ebe00d6e741093a467e10971c]:
   : Switched to a new branch 'fix-event-propagation'

5. I modify the source code.  For demo purposes I did that
   already.  Let’s copy a modified file into this directory:
   #+begin_src sh
     cp ~/src/wiwi-gitlab/misc/code/slickQuiz.js quiz/js
   #+end_src

   Let’s ask Git about the status of this project:
   #+begin_src sh :results output :exports code :cache yes
     git status
   #+end_src

   Output:
   #+RESULTS[5e6a5d78ce0eb21806597f1232088c6d75e1c4f9]:
   : On branch fix-event-propagation
   : Changes not staged for commit:
   :   (use "git add <file>..." to update what will be committed)
   :   (use "git checkout -- <file>..." to discard changes in working directory)
   :
   : 	     modified:   quiz/js/slickQuiz.js
   :
   : no changes added to commit (use "git add" and/or "git commit -a")

   Let’s see what I changed, i.e., the difference, or /diff/ proposed
   by me (similarly to differences we saw for the PR above):
   #+begin_src sh :results output :exports code :cache yes
     git diff
   #+end_src

   Shortened output (first 12 lines; initial lines encode what was
   compared; lines with ~@@~ indicate position of change, line with
   ~+~ indicates an added line; ~-~ would indicate a deleted line, but
   does not occur here):
   #+RESULTS[9101ef3b421652187caa27574c9eb60370455c77]:
   #+begin_example
   diff --git a/quiz/js/slickQuiz.js b/quiz/js/slickQuiz.js
   index 1711568..325680f 100644
   --- a/quiz/js/slickQuiz.js
   +++ b/quiz/js/slickQuiz.js
   @@ -698,6 +698,7 @@
                // Bind "start" button
                $quizStarter.on('click', function(e) {
                    e.preventDefault();
   +                e.stopPropagation();

                    if (!this.disabled && !$(this).hasClass('disabled')) {
                        plugin.method.startQuiz.apply (null, [{callback: plugin.config.animationCallbacks.startQuiz}]);
   #+end_example

6. Tell Git what changes to remember.  Recall that above we saw
   suggestions from ~git status~ how to proceed.  In general, it is a
   very good idea to read each output of Git carefully.  And to make
   sure that one understands it.  Web searches leading to
   StackOverflow usually help.  Let’s follow the suggestion:
   #+begin_src sh
     git add quiz/js/slickQuiz.js
   #+end_src

   #+begin_src sh :results output :exports code :cache yes
     git status
   #+end_src

   Output:
   #+RESULTS[99a6d2c18f03ea6e4a8ff9fa7c8ca40bcaa93845]:
   : On branch fix-event-propagation
   : Changes to be committed:
   :   (use "git reset HEAD <file>..." to unstage)
   :
   : 	modified:   quiz/js/slickQuiz.js

   Apparently, the status changed.  Git now tells us that changes are
   recorded to be /committed/.  Commit is the technical term for an
   operation that records changes permanently, along with an
   explanation for the changes.  Commit messages follow certain
   [[https://cbea.ms/git-commit/][conventions]], with a short first
   line, up to 50 characters, imperative, capitalized, and without
   dot; followed by an empty line; followed by a description where
   lines are wrapped at 72 characters.
   #+begin_src sh :results output :exports code :cache yes
     git commit -m "Prevent propagation of events

Reveal.js 4.2.0 introduced a new event handler, onSlidesClicked, which
breaks the hyperlinks used by the plugin (they now also trigger a
navigation to the presentation's first slide).
Therefore, stop the propagation of events."
   #+end_src

   Output:
   #+RESULTS[56d0a6cd1fea240bad1639f15f3cdbb3cd87cd3d]:
   :
   : [fix-event-propagation fcab02a] Prevent propagation of events
   :  1 file changed, 6 insertions(+)

   The status is clean again:
   #+begin_src sh :results output :exports code :cache yes
     git status
   #+end_src

   Output:
   #+RESULTS[99a6d2c18f03ea6e4a8ff9fa7c8ca40bcaa93845]:
   : On branch fix-event-propagation
   : nothing to commit, working tree clean

   My new commit is present in the log.
   #+begin_src sh :results output :exports code :cache yes
     git log -n 3
   #+end_src

   Output:
   #+RESULTS[85e0bcb5c717a27b51759d41db22126d197dac29]:
   #+begin_example
   commit fcab02a1b0f0f51d121f53c9b02f00f767e9271b (HEAD -> fix-event-propagation)
   Date:   Mon Mar 28 15:17:39 2022 +0200

       Prevent propagation of events

       Reveal.js 4.2.0 introduced a new event handler, onSlidesClicked, which
       breaks the hyperlinks used by the plugin (they now also trigger a
       navigation to the presentation's first slide).
       Therefore, stop the propagation of events.

   commit 602e918981acdf37aab5a71494b19a6317fd8ae0 (origin/master, origin/HEAD, master)
   Merge: 6fdb244 c7e2c86
   Date:   Tue Oct 1 17:25:09 2019 +0000

       Merge branch 'multiple-quizzes' into 'master'

       Allow multiple quizzes per presentation

       See merge request schaepermeier/reveal.js-quiz!11

   commit c7e2c86ad2f5cb20088229b1c19b1a602bad79f6 (origin/multiple-quizzes)
   Date:   Fri May 10 14:20:57 2019 +0200

       Fix initialization

       If no quiz has the name "quiz", the variable `quiz` is undefined.
   #+end_example

7. I /push/ my new local commit to the remote fork at GitLab:
   #+begin_src sh :eval never
     git push -u origin fix-event-propagation
   #+end_src

8. Let's see my fork at GitLab: https://gitlab.com/oer/reveal.js-quiz
   Note that GitLab identified the new branch and suggests that I create
   an MR.

   [[./gitlab-mr-suggest.png]]

9. Create MR.

   [[./gitlab-mr-create.png]]

That’s it for now.  When we create MRs, maintainers receive e-mail
notifications.  Then, it is up to them to review the changes.
Hopefully, maybe after some discussion, they merge the changes into
their stable version.  If that should not happen in this specific
case, I can still use my fork, which contains my changes.

Which free/libre and open source project do you want to improve next? 😃
